from os import path
import pyaudio
import pyttsx3
from pocketsphinx.pocketsphinx import *
import random

# startup phrases
init_phrases = [
    "Yes, sir?",
    "Yeah!",
    "Hey!",
    "Listening.",
    "What can I help you with?",
    "At your service!"
]

# failed phrases
failed_phrases = [
    "Sorry?",
    "Pardon?",
    "Excuse me?"
]

# TTS engine
engine = pyttsx3.init()

# setup for offline speech recognition
MODEL_DIR = "venv/Lib/site-packages/pocketsphinx/model"

config = Decoder.default_config()
config.set_string('-hmm', path.join(MODEL_DIR, 'en-us'))
config.set_string('-lm', path.join(MODEL_DIR, 'en-us.lm.bin'))
config.set_string('-dict', path.join(MODEL_DIR, 'cmudict-en-us.dict'))
config.set_string('-keyphrase', 'okay jarvis')
config.set_float('-kws_threshold', 1e+40)
config.set_string('-logfn','nul')
decoder = Decoder(config)

# mic input
p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=1024)


def listen():  # voice recognition offlien for wakeup word
    print "primed and ready...."
    stream.start_stream()
    in_speech_bf = False
    decoder.start_utt()
    while True:
        buf = stream.read(1024)
        if buf:
            decoder.process_raw(buf, False, False)
            # if decoder.hyp() is not None:
            #     print 'subtotal:', decoder.hyp().hypstr
            if decoder.get_in_speech() != in_speech_bf:
                in_speech_bf = decoder.get_in_speech()
                if not in_speech_bf and decoder.hyp().hypstr == 'okay jarvis':
                    print "heard my name..."
                    decoder.end_utt()
                    respond_init()
                    print "I spoke..."
                    processed_correctly = listen_for_command()
                    if processed_correctly is False:
                        respond_failed()
                    else:
                        listen()  # again for next command


def respond_init():  # offline response
    engine.say(random.choice(init_phrases))
    engine.runAndWait()


def respond_failed():
    print "I failed :("
    engine.say(random.choice(failed_phrases))
    engine.runAndWait()
    # restart the listener
    listen()


def listen_for_command():
    print "listening for further instructions...."

    

    return True


def main():  # start app
    listen()


if __name__ == '__main__':
    main()