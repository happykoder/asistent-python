#!/usr/bin/env python3
import speech_recognition as sr
import random
import pyttsx3


# startup phrases
init_phrases = [
    "Yes, sir?",
    "Yeah!",
    "Hey!",
    "Listening.",
    "What can I help you with?",
    "At your service!"
]

# failed phrases
failed_phrases = [
    "Sorry?",
    "Pardon?",
    "Excuse me?"
]

# obtain audio from the microphone
r = sr.Recognizer()


# TTS engine
engine = pyttsx3.init()

def listen():
 with sr.Microphone() as source:
    r.adjust_for_ambient_noise(source)
    print("Say something!")
    global audio
    while True:
        audio = r.listen(source)
        # recognize speech using Sphinx
        try:
            text = r.recognize_sphinx(audio)
            print("Sphinx thinks you said " + text)
            if text == 'hello':
                print "heard my name..."
                respond_init()
                print "I spoke..."
                processed_correctly = listen_for_command(r.listen(source))
                if processed_correctly is False:
                    respond_failed()
                else:
                    listen()  # again for next command
        except sr.UnknownValueError:
            print("Sphinx could not understand audio")
        except sr.RequestError as e:
            print("Sphinx error; {0}".format(e))

def listen_for_command(cmd):
    print "listening for further instructions...."

    # recognize speech using IBM Speech to Text
    IBM_USERNAME = "apikey"  # IBM Speech to Text usernames are strings of the form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
    IBM_PASSWORD = "SIcNNxLsu_KQRZDqALcQA8cmr5RDYkObL9xxdXLEpMut"  # IBM Speech to Text passwords are mixed-case alphanumeric strings
    try:
        print("IBM Speech to Text thinks you said " + r.recognize_ibm(cmd, username=IBM_USERNAME,
                                                                      password=IBM_PASSWORD))
    except sr.UnknownValueError:
        print("IBM Speech to Text could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from IBM Speech to Text service; {0}".format(e))

    return True



def respond_init():  # offline response
    engine.say(random.choice(init_phrases))
    engine.runAndWait()


def respond_failed():
    print "I failed :("
    engine.say(random.choice(failed_phrases))
    engine.runAndWait()
    # restart the listener
    listen()


def main():  # start app
    listen()


if __name__ == '__main__':
    main()